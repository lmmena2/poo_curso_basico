class Main {
    public static void main(String[] args) {
        UberX uberX = new UberX("LBA1234", new Account("Luis Mena","1104045461"),"Kia","rio");
        uberX.setPassenger(4);
        uberX.printDataCar();

        UberVan uberVan = new UberVan("LBC1234", new Account("Luis Mena","1104045461"));
        uberVan.setPassenger(6);
        uberVan.printDataCar();
    }
}